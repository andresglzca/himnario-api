'use strict'

const express = require('express')

var cors = require('cors');

const app = express()
const api = require('./routes')

app.use(express.urlencoded({ extended: false }))
app.use(express.json());
app.use(cors());

app.use('/api', api)

module.exports = app