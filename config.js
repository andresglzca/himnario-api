const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    port: process.env.PORT,
    db: process.env.MONGODB_URI,
    mongoUsername: process.env.MONGO_USER,
    mongoPassword: process.env.MONGO_PWD,
    mongoAuthSource: process.env.MONGO_AUTHSOURCE,
    SECRET_TOKEN: 'miclavedetokens'
  }