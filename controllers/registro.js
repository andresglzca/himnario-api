"use strict";

const himnos = require("../models/registro");

const myCustomLabels = {
  totalDocs: "itemCount",
  docs: "himnos",
  page: "currentPage",
};

function query(req, res) {
  let searchTerm;
  searchTerm = req.params.searchTerm;
  console.info("parametro de busqueda: ", searchTerm);
  if (searchTerm) {
    let query = himnos
      .find({
        $text: { $search: `\"${searchTerm}\"`, $diacriticSensitive: false },
      })
      .sort({ number: "asc" });

    himnos
      .paginate(query, {
        pagination: false,
        customLabels: myCustomLabels,
      })
      .then((result) => {
        if (result.himnos.length) {
          console.info("Se ejecuto correctamente la busqueda");
          return res.status(200).json(result);
        } else {
          return res.status(404).send({ message: "No se encontraron himnos" });
        }
      })
      .catch((error) => {
        console.error(`Error al realizar la petición de busqueda: ${error}`);
        return res
          .status(500)
          .send({
            message: `Error al realizar la petición de busqueda: ${error}`,
          });
      });
  } else {
    res.status(400).send({
      message: `Parametro de busqueda vacio ej: /api/himnos/buscar/gloria`,
    });
  }
}

function getHimnos(req, res) {
  let page;
  page = req.params.page;
  if (!page) page = 1;
  himnos
    .paginate(
      {},
      {
        page: page,
        limit: 10,
        sort: { number: "asc" },
        customLabels: myCustomLabels,
      }
    )
    .then((result) => {
      if (himnos.length) {
        console.info("Se ejecuto correctamente");
        return res.status(200).json(result);
      } else {
        return res.status(404).send({ message: "No existen himnos" });
      }
    })
    .catch((error) => {
      console.error(`Error al realizar la petición: ${error}`);
      return res
        .status(500)
        .send({ message: `Error al realizar la petición: ${error}` });
    });
}

function getHimno(req, res) {
  let himno;
  himno = req.params.himno;
  if (!himno) himno = 1;
  himnos
    .paginate(
      {},
      {
        page: himno,
        limit: 1,
        sort: { number: "asc" },
        customLabels: myCustomLabels,
      }
    )
    .then((result) => {
      if (himnos.length) {
        console.info("Se ejecuto correctamente");
        return res.status(200).json(result);
      } else {
        return res.status(404).send({ message: "No existen himnos" });
      }
    })
    .catch((error) => {
      console.error(`Error al realizar la petición: ${error}`);
      return res
        .status(500)
        .send({ message: `Error al realizar la petición: ${error}` });
    });
}

module.exports = {
  query,
  getHimnos,
  getHimno
};
