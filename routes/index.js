  'use strict'

const express = require('express')
const registroCtrl = require('../controllers/registro')
const api = express.Router()

api.get('/himno/:himno?', registroCtrl.getHimno)
api.get('/himnos/:page?', registroCtrl.getHimnos)
api.get('/himnos/buscar/:searchTerm?', registroCtrl.query)

module.exports = api