'use strict'

const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema

const himnosSchema = Schema({
    number: Number,
    titulo: String,
    texto: String,
    contenido: String,
    autor: String,
    audio: String,
    pdf: String
});

himnosSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('himnos', himnosSchema)