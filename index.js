"use strict";

const mongoose = require("mongoose");
const app = require("./app");
const config = require("./config");

console.log(config);
mongoose.connect(
  config.db,
  {
    authSource: config.mongoAuthSource,
    user: config.mongoUsername,
    pass: config.mongoPassword,
    useUnifiedTopology: true,
    useNewUrlParser: true,
  },
  (err, res) => {
    if (err) {
      return console.log(`Error al conectar a la base de datos: ${err}`);
    }
    console.log("Conexión a la base de datos establecida...");

    app.listen(config.port, () => {
      console.log(`API corriendo en http://localhost:${config.port}`);
    });
  }
);
